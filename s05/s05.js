class Customer {
    constructor(email){
        this.email = email
        this.cart = new Cart()
        this.orders = []
    }

    checkOut(){
        if(this.cart.contents.length > 0){
            this.orders.push({
                products: this.cart.contents,
                totalAmount: this.cart.computeTotal().totalAmount
            })
        }
        return this
    }
}

class Product {
    constructor(name, price){
        this.name = name
        this.price = price
        this.isActive = true
    }

    archive(){
        if(this.isActive === true){
            this.isActive = false
        }
        return this
    }

    updatePrice(newPrice){
        this.price = newPrice
        return this
    }
}

class Cart {
    constructor(){
        this.contents = []
        this.totalAmount = 0
    }

    addToCart(product, quantity){
        this.contents.push({
            product: product,
            quantity: quantity
        })
        return this
    }

    showCartContents(){
        console.log(this.contents)
        return this
    }

    updateProductQuantity(name, quantity){
        this.contents.find(content => content.product.name === name).quantity = quantity
        return this
    }

    clearCartContents(){
        this.contents = []
        return this
    }

    computeTotal(){
        if(this.contents.length > 0){
            this.contents.forEach(content => {
                this.totalAmount = this.totalAmount + (content.product.price * content.quantity)
            })
        }
        return this
    }
}

//test statements
// const john = new Customer('john@mail.com')

// const prodA = new Product('soap', 9.99)
// const prodB = new Product('shampoo', 12.99)
// const prodC = new Product('toothbrush', 4.99)
// const prodD = new Product('toothpaste', 14.99)

// john.cart.addToCart(prodA, 3)
// john.cart.addToCart(prodB, 2)

// john.cart.computeTotal()
// john.cart.updateProductQuantity('soap', 5)
// john.cart.showCartContents()
// john.cart.clearCartContents()
// john.cart.showCartContents()
// john.checkOut()