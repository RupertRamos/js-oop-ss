//1. 
const addToEnd = (arr, element) => {
    if(typeof element !== "string"){
        return "error - can only add strings to an array";
    }
    arr.push(element);
    return arr;
}

//test input
addToEnd(students, "Ryan"); //["John", "Joe", "Jane", "Jessie", "Ryan"]
//validation check
addToEnd(students, 045); //"error - can only add strings to an array"


//2.
const addToStart = (arr, element) => {
    if(typeof element !== "string"){
        return "error - can only add strings to an array";
    }
    arr.unshift(element);
    return arr;
}

//test input
addToStart(students, "Tess"); //["Tess", "John", "Joe", "Jane", "Jessie", "Ryan"]
//validation check
addToStart(students, 033); //"error - can only add strings to an array"


// 3.
const elementChecker = (arr, elementToBeChecked) => {
    if(arr.length === 0){
        return "error - passed in array is empty";
    }
    return arr.some(element => element === elementToBeChecked);    
}

//test input
elementChecker(students, "Jane"); //true
//validation check
elementChecker([], "Jane"); //"error - passed in array is empty"

//4.
const checkAllStringsEnding = (arr, char) => {
    if(arr.length === 0) return "error - array must NOT be empty";
    if(arr.some(element => typeof element !== "string")) return "error - all array elements must be strings";
    if(typeof char !== "string") return "error - 2nd argument must be of data type string";
    if(char.length > 1) return "error - 2nd argument must be a single character";
    return arr.every(element => element[element.length-1] === char);
}

//test input
checkAllStringsEnding(students, "e"); //false
//validation checks
checkAllStringsEnding([], "e"); //"error - array must NOT be empty"
checkAllStringsEnding(["Jane", 02], "e"); //"error - all array elements must be strings"
checkAllStringsEnding(students, 4); //"error - 2nd argument must be of data type string"
checkAllStringsEnding(students, "el"); //"error - 2nd argument must be a single character"

//5.
const stringLengthSorter = (arr) => {
    if(arr.some(element => typeof element !== "string")) return "error - all array elements must be strings";
    arr.sort((elementA, elementB) => {
        return elementA.length - elementB.length;
    })
    return arr;
}
//test input
stringLengthSorter(students); //["Joe", "Tess", "John", "Jane", "Ryan", "Jessie"]
//validation check
stringLengthSorter([037, "John", 039, "Jane"]); //"error - all array elements must be strings"

//6.
const startsWithCounter = (arr, char) => {
    if(arr.length === 0) return "error - array must NOT be empty";
    if(arr.some(element => typeof element !== "string")) return "error - all array elements must be strings";
    if(typeof char !== "string") return "error - 2nd argument must be of data type string";
    if(char.length > 1) return "error - 2nd argument must be a single character";
    let result = 0;
    arr.forEach(element => {
        if(element[0].toLowerCase() === char.toLowerCase()) result++;
    })
    return result;
}
//test input
startsWithCounter(students, "j"); //4


// 7. 
const likeFinder = (arr, str) => {
    if(arr.length === 0) return "error - array must NOT be empty";
    if(arr.some(element => typeof element !== "string")) return "error - all array elements must be strings";
    if(typeof str !== "string") return "error - 2nd argument must be of data type string";
    let result = [];
    arr.forEach(element => {
        if(element.toLowerCase().includes(str.toLowerCase())) result.push(element);
    })
    return result;
}
//test input
likeFinder(students, "jo"); //["Joe", "John"]

//8.
const randomPicker = (arr) => {
    if(arr.length === 0) return "error - array must NOT be empty";
    return arr[Math.floor(Math.random() * arr.length)];
}
//test input
randomPicker(students); //"Ryan"
randomPicker(students); //"John"
randomPicker(students); //"Jessie"



