
let students = ["John", "Joe", "Jane", "Jessie"];

console.log(students);


Math.E        // returns Euler's number
Math.PI       // returns PI
Math.SQRT2    // returns the square root of 2
Math.SQRT1_2  // returns the square root of 1/2
Math.LN2      // returns the natural logarithm of 2
Math.LN10     // returns the natural logarithm of 10
Math.LOG2E    // returns base 2 logarithm of E
Math.LOG10E   // returns base 10 logarithm of E


// methods for rounding a number to an integer
Math.round(3.14) //3 - rounds to nearest integer
Math.ceil(3.14) //4 - rounds UP to nearest integer
Math.floor(3.14) //3 - rounds DOWN to nearest integer
Math.trunc(3.14) //3 - returns only the integer part (NEW in ES6)

// method for returning the square root of a number
Math.sqrt(3.14) //1.77

// method for finding the lowest value in a list of arguments
Math.min(-4, -3, -2, -1, 0, 1, 2, 3, 4) //-4

// method for finding the highest value in a list of arguments
Math.max(-4, -3, -2, -1, 0, 1, 2, 3, 4) //4

// method for returning a random number between 0 (inclusive) and 1 (exclusive)
Math.random()


// [SECTION] Objects vs. Primitives

let name = "mario";
console.log(name);
console.log(name.length);
console.log(name.toUpperCase());s