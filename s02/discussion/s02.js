//Creation of objects in JS is done via the object literal: {} (curly braces)
//Recall, an object is a data structure that contains a collection of key-value pairs.
//A key is also called a property and is used to identify the characteristics of the object
//The value corresponding to each key/property can be 
//  1. A simple value
//  2. An array of values
//  3. Objects
//  4. Functions
//When a function is created inside an object, it is called a method of that object.

//Create an object for the first student
let studentOne = {
    name: 'John', //Each key-value pair is formatted as key : value, and each key-value pair is separated by commas.
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],
    //add the functionalities available to a student as object methods
        //the keyword "this" refers to the object encapsulating the method where "this" is called
    login(){
        //this.email implies studentOne.email
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    //Mini exercise:
    //Create function that will get the quarterly average of studentOne's grades
    computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
        //Q: why did I use 4 instead of sales.length?
        //A: because there are 4 quarters in a year
    },

    //Mini exercise 2:
    //Create function that will return true if average grade is >=85, false otherwise
    willPass() {
        //hint: you can call methods inside an object
        return this.computeAve() >= 85 ? true : false
        //the ternary operator used in this solution is equivalent to:
        /* if(this.computeAve() >= 85){
            return true;
        } else {
            return false;
        } */
        //the syntax of a ternary statement is as follows:
        //condition ? value if condition is true : value if condition is false
    },

    //Mini exercise 3:
    //Create a function called willPassWithHonors() that returns true if the student has passed and their average grade is >= 90. The function returns false if either one is not met.
    willPassWithHonors() {
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    }
}

//Whenever we add properties or methods to an object, we are organizing information (properties)
//and behaviors (methods) that belong to that object. We call this process as ENCAPSULATION.

//log the content of studentOne's encapsulated information in the console
//console.log(`student one's name is ${studentOne.name}`)
//console.log(`student one's email is ${studentOne.email}`)
//console.log(`student one's quarterly grade averages are ${studentOne.grades}`)

//Exercise: Using the information from s02boilerplate.js, create the other 3 students along with the methods created in studentOne
//Expected output: 4 student objects (John, Joe, Jane, Jessie)

let studentTwo = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78, 82, 79, 85],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass() {
        return this.computeAve() >= 85 ? true : false;
    },
    willPassWithHonors() {
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    }
}

let studentThree = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87, 89, 91, 93],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass() {
        return this.computeAve() >= 85 ? true : false;
    },
    willPassWithHonors() {
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    }
}

let studentFour = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass() {
        return this.computeAve() >= 85 ? true : false;
    },
    willPassWithHonors() {
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    }
}

const classOf1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],
    countHonorStudents() {
        let result = 0;
        this.students.forEach(student => {
            if (student.willPassWithHonors()) {
                result++;
            }
        })
        return result;
    },
    honorsPercentage() {
        return (this.countHonorStudents() / this.students.length) * 100;
    },
    retrieveHonorStudentInfo() {
        let honorStudents = [];
        this.students.forEach(student => {
            if(student.willPassWithHonors()) honorStudents.push({
                email: student.email,
                aveGrade: student.computeAve()
            })
        })
        return honorStudents;
    },
    sortHonorStudentsByGradeDesc(){
        return this.retrieveHonorStudentInfo().sort((studentA, studentB) => {
            return studentB.aveGrade - studentA.aveGrade;
        })
    }
}